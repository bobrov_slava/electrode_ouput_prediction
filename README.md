## Electrode output predictor

Predicts theta output of a single electrode based on theta, beta and SMR input from 3 neighbour electrodes.


1. Run WebServer.py which will start a TCP Server on 8765 port by defalut
2. Send a json data input from 3 electrodes (format described below)
3. You will receive prediction of theta for 4th electrode


---

**Example of json data**  
{  
"data_points":[{  
    "electrode0": [1, 2, 3],  
    "electrode1": [1, 2, 3],  
    "electrode2": [1, 2, 3]  
  },{   
    "electrode0": [1, 2, 3],  
    "electrode1": [1, 2, 3],  
    "electrode2": [1, 2, 3]  
  }]    
}  