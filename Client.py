import socket
import json

data = '''
{"data_points":[{
    "electrode0": [1, 2, 3],
    "electrode1": [1, 2, 3],
    "electrode2": [1, 2, 3]
  },{  
    "electrode0": [1, 2, 3],
    "electrode1": [1, 2, 3],
    "electrode2": [1, 2, 3]
  }]  
 }'''

HOST = '127.0.0.1'
PORT = 65432

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    for _ in range(10):
        send_data = data.encode()
        s.send(send_data)
        received_data = s.recv(1024)
        received_data = received_data.decode()
        json_data = json.loads(received_data)
        print(json_data)
