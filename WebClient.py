import asyncio
import websockets

data = '''
{"data_points":[{
    "electrode0": [1, 2, 3],
    "electrode1": [1, 2, 3],
    "electrode2": [1, 2, 3]
  },{  
    "electrode0": [1, 2, 3],
    "electrode1": [1, 2, 3],
    "electrode2": [1, 2, 3]
  }]  
 }'''

import time
async def hello():
    async with websockets.connect(
            'ws://127.0.0.1:8765') as websocket:
        await websocket.send(data)
        print(f"> {data}")

        predictions = await websocket.recv()
        print(f"< {predictions}")
        time.sleep(10)



asyncio.get_event_loop().run_until_complete(hello())
