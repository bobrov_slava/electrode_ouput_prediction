import socket
import json
import numpy as np
import pandas as pd
from ElectordePredictor import read_electrodes_data, split_data_train_test, SequencePredictor


class ElectrodeServer:

    def __init__(self, predictor, host='127.0.0.1', port=65432):
        self.host = host
        self.port = port
        self.predictor = predictor

    def convert_to_matrix(self, json_data):
        data_array = json_data['data_points']
        n_rows = len(data_array)
        matrix_data = np.empty((n_rows, 9))
        for i in range(n_rows):
            electrode0 = data_array[i]['electrode0']
            electrode1 = data_array[i]['electrode1']
            electrode2 = data_array[i]['electrode2']
            data_point = np.concatenate((electrode0, electrode1, electrode2))
            matrix_data[i] = data_point
        return matrix_data

    def start_tcp(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_listener:
            socket_listener.bind((self.host, self.port))
            print('Server has been started on {0}:{1}'.format(self.host, self.port))
            socket_listener.listen()
            connection, address = socket_listener.accept()
            with connection:
                print('Connected by', address)
                while True:
                    received_data = connection.recv(4096)
                    received_data.decode()
                    if received_data:
                        json_data = json.loads(received_data)
                        matrix_data = self.convert_to_matrix(json_data)
                        predictions = self.predictor.predict_sequence(matrix_data, n_steps_forward=1)
                        json_predictions = pd.Series(predictions.ravel()).to_json(orient='values')
                        connection.sendall(json_predictions.encode())


if __name__ == '__main__':
    print('Reading training data')
    electrodes_data = read_electrodes_data('out.csv')
    (train_features_data, train_target_data), (test_features_data, test_target_data) = split_data_train_test(
        electrodes_data)

    n_inputs = train_features_data.shape[1]
    n_outputs = train_target_data.shape[1]

    print('Starting training of model')
    predictor = SequencePredictor(n_inputs, n_outputs)
    predictor.train(train_features_data,
                    train_target_data,
                    test_features_data,
                    test_target_data,
                    sequence_length=300,
                    epochs=1)
    print('Model has been trained')

    server = ElectrodeServer(predictor)
    server.start_tcp()
