import numpy as np
import pandas as pd
import tensorflow as tf
from keras.layers import GRU, Dense
from keras.models import Sequential
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler


def read_electrodes_data(filepath):
    df = pd.read_csv(filepath)
    df_electrode0 = df.loc[df['electrode'] == 0][['theta', 'smr', 'beta']].reset_index(drop=True)
    df_electrode1 = df.loc[df['electrode'] == 1][['theta', 'smr', 'beta']].reset_index(drop=True)
    df_electrode2 = df.loc[df['electrode'] == 2][['theta', 'smr', 'beta']].reset_index(drop=True)
    df_electrode3 = df.loc[df['electrode'] == 3][['theta', 'smr', 'beta']].reset_index(drop=True)
    df_e0_e1 = df_electrode0.join(df_electrode1, how='outer', lsuffix='_e0', rsuffix='_e1')
    df_e2_e3 = df_electrode2.join(df_electrode3, how='outer', lsuffix='_e2', rsuffix='_e3')
    df_electrodes = df_e0_e1.join(df_e2_e3, how='outer')[:-1]  # 3rd electrode doesn't have 5622nd data point
    return df_electrodes


def split_data_train_test(df_electrodes, test_split=0.1):
    features = ['theta_e0', 'smr_e0', 'beta_e0', 'theta_e1', 'smr_e1', 'beta_e1', 'theta_e2', 'smr_e2', 'beta_e2']
    features_data = df_electrodes[features].values
    target_data = df_electrodes[['theta_e3']].values
    train_features_data, test_features_data, train_target_data, test_target_data = \
        train_test_split(features_data, target_data, test_size=test_split, random_state=42)
    return (train_features_data, train_target_data), (test_features_data, test_target_data)


class SequencePredictor:

    def __init__(self, n_features, n_outputs):
        self.n_features = n_features
        self.n_outputs = n_outputs
        self.model = self._build_model()

    def _build_model(self):
        model = Sequential()
        model.add(GRU(32, input_shape=(None, self.n_features)))
        model.add(Dense(self.n_outputs, activation='sigmoid'))
        model.compile(optimizer="RMSProp", loss=self._mse_sequence_loss)
        model.summary()
        return model

    def _mse_sequence_loss(self, y_true, y_pred):
        loss = tf.losses.mean_squared_error(labels=y_true, predictions=y_pred)
        return tf.reduce_mean(loss)

    def _batch_generator(self, features_data, target_data, sequence_length, batch_size):
        max_idx_range = features_data.shape[0] - sequence_length - 1

        while True:
            train_batch = np.zeros((batch_size, sequence_length, self.n_features))
            test_batch = np.zeros((batch_size, self.n_outputs))

            for i in range(batch_size):
                idx = np.random.randint(max_idx_range)
                train_batch[i] = features_data[idx:idx + sequence_length]
                test_batch[i] = target_data[idx + sequence_length]
            yield train_batch, test_batch

    def _preprocess_data(self, train_features_data, train_target_data, test_features_data, test_target_data):
        """ Gradient descent works better with scaled values [0;1]"""
        self.feature_data_scaler = MinMaxScaler()
        train_features_data = self.feature_data_scaler.fit_transform(train_features_data)
        test_features_data = self.feature_data_scaler.transform(test_features_data)

        self.target_data_scaler = MinMaxScaler()
        train_target_data = self.target_data_scaler.fit_transform(train_target_data)
        test_target_data = self.target_data_scaler.transform(test_target_data)
        return train_features_data, train_target_data, test_features_data, test_target_data

    def train(self,
              train_features_data,
              train_target_data,
              test_features_data,
              test_target_data,
              sequence_length,
              epochs=10,
              steps_per_epoch=50,
              batch_size=64):

        train_features_data, train_target_data, test_features_data, test_target_data = \
            self._preprocess_data(train_features_data, train_target_data, test_features_data, test_target_data)
        train_generator = self._batch_generator(train_features_data, train_target_data, sequence_length, batch_size)
        validation_generator = self._batch_generator(test_features_data, test_target_data, sequence_length, batch_size)

        self.model.fit_generator(generator=train_generator,
                                 epochs=epochs,
                                 steps_per_epoch=steps_per_epoch,
                                 validation_steps=steps_per_epoch,
                                 validation_data=validation_generator,
                                 verbose=2)

    def predict(self, sequence_data):
        sequence_data = np.expand_dims(sequence_data, axis=0)
        return self.model.predict(sequence_data)

    # rewrite to predict based on sequence, not prediction/generation
    def predict_sequence(self, sequence_data, n_steps_forward):
        input_next = self.feature_data_scaler.transform(sequence_data)
        predictions = np.empty((n_steps_forward,))

        for i in range(n_steps_forward):
            y_predicted_next = self.predict(input_next)
            input_next[:-1, :] = input_next[1:, :]
            input_next[-1, 0] = y_predicted_next[0]
            predictions[i] = y_predicted_next[0]
        return self.target_data_scaler.inverse_transform(predictions.reshape(-1, 1))


if __name__ == '__main__':
    electrodes_data = read_electrodes_data('out.csv')
    (train_features_data, train_target_data), (test_features_data, test_target_data) = split_data_train_test(
        electrodes_data)

    n_inputs = train_features_data.shape[1]
    n_outputs = train_target_data.shape[1]

    predictor = SequencePredictor(n_inputs, n_outputs)
    predictor.train(train_features_data,
                    train_target_data,
                    test_features_data,
                    test_target_data,
                    sequence_length=300,
                    epochs=1)
    predictions = predictor.predict_sequence(test_features_data[:10], n_steps_forward=1)
    print(predictions)
